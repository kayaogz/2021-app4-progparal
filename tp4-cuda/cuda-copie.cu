#include <cstdio>
#include <iostream>
#include "cuda.h"

using namespace std;

__global__ void cudaCopieParBlocs(float *tab0, const float *tab1, int taille)
{
  int idx;
  // Calculer le bon idx en fonction du blockIdx.x, threadIdx.x, et blockDim.x
  // A FAIRE ...
  // idx = ?
  if (idx < taille) { tab0[idx] = tab1[idx]; }
}

__global__ void cudaCopieParBlocsThreads(float *tab0, const float *tab1, int taille)
{
  int idx; // idx = ?
  // Calculer le bon idx en fonction du blockIdx.x, threadIdx.x, et blockDim.x
  // A FAIRE ...
  // idx = ?
  if (idx < taille) { tab0[idx] = tab1[idx]; }
}

int main(int argc, char **argv) {
  float *A, *B, *Ad, *Bd;
  int N, i;

  if (argc < 2) {
    printf("Utilisation: ./cuda-copie N\n");
    return 0;
  }
  N = atoi(argv[1]);

  // Initialisation
  A = (float *) malloc(sizeof(float) * N);
  B = (float *) malloc(sizeof(float) * N);
  for (i = 0; i < N; i++) { 
    A[i] = (float)i;
    B[i] = 0.0f;
  }
  
  // Allouer les tableau Ad et Bd dynamiques de taille N sur le GPU avec cudaMalloc 
  // A FAIRE ...

  // Copier A dans Ad et B dans Bd
  // A FAIRE ...

  // Copier Ad dans Bd avec le kernel cudaCopieParBlocs
  // A FAIRE ...
  // cudaCopieParBlocs<<<...,...>>>(...) ???

  // Attendre que le kernel cudaCopieParBlocs termine
  cudaError_t cudaerr = cudaDeviceSynchronize();
  if (cudaerr != cudaSuccess) {
    printf("L'execution du kernel a echoue avec le code d'erreur \"%s\".\n", cudaGetErrorString(cudaerr));
  }

  // Copier Bd dans B pour la verification
  // A FAIRE ...

  // Verifier le resultat en CPU en comparant B avec A
  for (i = 0; i < N; i++) { if (A[i] != B[i]) { break; } }
  if (i < N) { cout << "La copie est incorrecte!\n"; }
  else { cout << "La copie est correcte!\n"; }

  // Remettre B a zero puis recopier dans Bd tester le deuxieme kernel de copie
  for (int i = 0; i < N; i++) { B[i] = 0.0f; }
  // A FAIRE ...

  // Copier Ad dans Bd avec le kernel cudaCopieParBlocsThreads
  // A FAIRE ...
  // cudaCopieParBlocsThreads<<<...,...>>>(...) ???

  // Attendre que le kernel cudaCopieParBlocsThreads termine
  cudaerr = cudaDeviceSynchronize();
  if (cudaerr != cudaSuccess) {
    printf("L'execution du kernel a echoue avec le code d'erreur \"%s\".\n", cudaGetErrorString(cudaerr));
  }

  // Copier Bd dans B pour la verification
  // A FAIRE ...

  // Verifier le resultat en CPU en comparant B avec A
  for (i = 0; i < N; i++) { if (A[i] != B[i]) { break; } }
  if (i < N) { cout << "La copie est incorrecte!\n"; }
  else { cout << "La copie est correcte!\n"; }

  // Desaollouer le tableau Ad[N] et Bd[N] sur le GPU
  // A FAIRE ...

  // Desallouer A et B
  free(A);
  free(B);

  return 0;
}
