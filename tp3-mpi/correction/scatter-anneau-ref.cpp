#include <iostream>
#include <vector>
#include "mpi.h"

using namespace std;

int MPI_ScatterIntAnneau(
    const int *sendbuf,
    int count,
    int *recvbuf,
    int root,
    MPI_Comm comm)
{
  // A FAIRE ...
  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  std::vector<int> temp(count);
  if (size > 1) {
    int prev = (rank + size - 1) % size;
    int next = (rank + 1) % size;
    int rangDansAnneau;
    if (rank >= root) { rangDansAnneau = rank - root; }
    else { rangDansAnneau = size - root + rank; }
    int numCommEtapes = size - rangDansAnneau;
    if (rank == root) {
      for (int i = prev; i != root; i = (i - 1 + size) % size) {
        MPI_Send(&sendbuf[i * count], count, MPI_INT, next, 0, comm);
      }
    } else {
      for (int i = 0; i < numCommEtapes; i++) {
        MPI_Recv(&recvbuf[0], count, MPI_INT, prev, 0, comm, MPI_STATUS_IGNORE);
        if (i < numCommEtapes - 1 && next != root) {
          // Avant root fait un recv et non pas send
          // Dans la derniere etape, chaque process aura son propre message, donc pas de send a effectuer
          MPI_Send(&recvbuf[0], count, MPI_INT, next, 0, comm);
        }
      }
    }
  }
  if (rank == root) {
    memcpy(recvbuf, &sendbuf[root * count], count * sizeof(int));
  }

  return MPI_SUCCESS;
} 

int main(int argc, char **argv)
{
  MPI_Init(&argc, &argv);
  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  int elemsParProc = 5; // Taille de chaque petit tableau Aloc par processus apres Scatter
  int root = 1; // Processus root qui possede le tableau entier
  std::vector<int> Aloc(elemsParProc); // Petit tableau par processus
  std::vector<int> A; // Grand tableau, a allouer et initialiser dans le processus root
  if (rank == root) { 
    A.resize(elemsParProc * size);
    for (int i = 0; i < elemsParProc * size; i++) { A[i] = i; }
  }
  
  // Appeler MPI_ScatterIntAnneau(...) sur A et Aloc, et avec la root. Tester contre MPI_Scatter(...)
  // A FAIRE ...
  MPI_ScatterIntAnneau(&A[0], elemsParProc, &Aloc[0], root, MPI_COMM_WORLD);
//  MPI_Scatter(&A[0], elemsParProc, MPI_INT, &Aloc[0], elemsParProc, MPI_INT, root, MPI_COMM_WORLD);

  // Afficher Aloc pour chaque processus pour tester
  for (auto i : Aloc) { cout << i << ":" << rank << endl; }

  MPI_Finalize();

  return 0;
}
