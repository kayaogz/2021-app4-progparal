#include <iostream>
#include <vector>
#include "mpi.h"

using namespace std;

int MPI_ScatterIntAnneau(
    const int *sendbuf,
    int count,
    int *recvbuf,
    int root,
    MPI_Comm comm)
{
  // A FAIRE ...
  return MPI_SUCCESS;
} 

int main(int argc, char **argv)
{
  MPI_Init(&argc, &argv);
  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  int elemsParProc = 5; // Taille de chaque petit tableau Aloc par processus apres Scatter
  int root = 1; // Processus root qui possede le tableau entier
  std::vector<int> Aloc(elemsParProc); // Petit tableau par processus
  std::vector<int> A; // Grand tableau, a allouer et initialiser dans le processus root
  if (rank == root) { 
    A.resize(elemsParProc * size);
    for (int i = 0; i < elemsParProc * size; i++) { A[i] = i; }
  }
  
  // Appeler MPI_ScatterIntAnneau(...) sur A et Aloc, et avec la root. Tester contre MPI_Scatter(...)
  // A FAIRE ...
//  MPI_ScatterIntAnneau(...);
//  MPI_Scatter(...);

  // Afficher Aloc pour chaque processus pour tester
  for (auto i : Aloc) { cout << i << ":" << rank << endl; }

  MPI_Finalize();

  return 0;
}
